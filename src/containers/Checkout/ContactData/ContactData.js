import React,{ Component } from "react";
import {connect} from 'react-redux';

import Button from '../../../components/UI/Button/Button';
import classes from './ContactData.css';
import axios from '../../../axios-orders';
import Spinner from '../../../components/UI/Spinner/Spinner';
import Input from '../../../components/UI/Input/Input';
import withErrorHandler from '../../../hoc/withErrorHandler/withErrorHandler';
import * as actions from '../../../store/actions/index';
import {updateObject,checkValidity} from '../../../shared/utility';

class ContactData extends Component{
    state = {
        orderForm: {
            name : {
                elementType : 'input',
                elementConfig: {
                    type :'text',
                    placeholder:'your name'
                },
                value : '',
                validation : {
                    required : true
                },
                valid :false,
                touched :false
            },
            street : {
                elementType : 'input',
                elementConfig: {
                    type :'text',
                    placeholder:'your street'
                },
                value : '',
                validation : {
                    required : false
                },
                valid :true,
                touched :false
            },
            zipCode : {
                elementType : 'input',
                elementConfig: {
                    type :'text',
                    placeholder:'your postal code'
                },
                value : '',
                validation : {
                    required : true,
                    minLength : 2
                },
                valid :false,
                touched :false
            },
            country : {
                elementType : 'input',
                elementConfig: {
                    type :'text',
                    placeholder:'your country'
                },
                value : '',
                validation : {
                    required : false
                },
                valid :true,
                touched :false
            },
            email : {
                elementType : 'input',
                elementConfig: {
                    type :'email',
                    placeholder:'your email'
                },
                value : '',
                validation : {
                    required : false,
                    touched :false
                },
                valid :true
            },
  
            deliveryMethod : {
                elementType : 'select',
                elementConfig: {
                    options : [
                        {value : 'fastest', displayValue : 'Fastest'},
                        {value : 'chepeast', displayValue : 'Chepeast'}
                    ]
                },
                value : 'fastest',
                validation : {},
                valid :true
            }
        },
        formIsValid : false
    }

  

    orderHandler = (event)=> {
        event.preventDefault();

         const formData = {}

        for (let i in this.state.orderForm){
            formData[i]= this.state.orderForm[i].value
        }
         
        const order = {
            ingredients : this.props.ings,
            price : this.props.price,
            orderData : formData,
            userId:this.props.userId

        }
        this.props.onOrderBurger(order,this.props.token);


    }

    inputChangedHandler =(event, inputIdentifier) => {

        const updatedFormElement =  updateObject(this.state.orderForm[inputIdentifier],{
            value :event.target.value,
            valid :checkValidity(event.target.value, this.state.orderForm[inputIdentifier].validation),
            touched : true
        }) ;

        const updatedOrderForm = updateObject(this.state.orderForm,{
            [inputIdentifier] : updatedFormElement
        });

        let formIsValid = true;
        for (let inputIdentifier in updatedOrderForm){
            formIsValid = updatedOrderForm[inputIdentifier].valid && formIsValid;
        }

        this.setState({orderForm : updatedOrderForm, formIsValid:formIsValid});

    }

    render() {
        const formElementsArray = [];
        for (let key in this.state.orderForm){
            formElementsArray.push({
                id : key,
                config:this.state.orderForm[key]
            });
        }
        let form =(
            <form onSubmit={this.orderHandler}>
                {formElementsArray.map(formElement => (
                    <Input 
                        key = {formElement.id}
                        invalid = {!formElement.config.valid}
                        touched={formElement.config.touched}
                        shouldValidate = {formElement.config.validation}
                        changed = {(event) => this.inputChangedHandler(event,formElement.id)}
                        inputType={formElement.config.elementType}
                        elementConfig={formElement.config.elementConfig}
                        value={formElement.config.value}
                    />
                ))}
                <Button btnType="Success" disabled={!this.state.formIsValid}>Order</Button>
        </form>
        );
        if (this.props.loading){
            form = <Spinner />
        }
        return (
            <div className={classes.ContactData}>
                <h4>Enter contact data</h4>
                {form}
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        ings : state.burgerBuilder.ingredients,
        price : state.burgerBuilder.totalPrice,
        loading : state.order.loading,
        token : state.auth.token,
        userId : state.auth.userId
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onOrderBurger : (orderData,token) => dispatch(actions.purchaseBurger(orderData,token))
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(withErrorHandler(ContactData,axios) );