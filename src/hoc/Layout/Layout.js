import React, { Component } from 'react';
import {connect} from 'react-redux';
import Aux from '../Auxi/Auxi';
import classes from './Layout.css'
import Toolbar from '../../components/Navigation/Toolbar/Toolbar';
import SideDrawer from '../../components/Navigation/SideDrawer/SideDrawer';


class Layout extends Component {
    state = {
        showSideDrawer : false
    }

    sideDrawerClosedHandler = (props) => {
        this.setState({showSideDrawer :false})
    }

    toggleSideDrawerHandler = () => {
       // rappel ne pas faire comme ça : asynchrone the last state maybe
        //this.setState({showSideDrawer : !this.showSideDrawer});
        this.setState((prevState)=>{
            return {showSideDrawer : !prevState.showSideDrawer};
        }); 
            
    }

    render() {

        return (
            <Aux>
                <Toolbar 
                    isAuth = {this.props.isAuthenticated}
                    drawerToggleClicked={this.toggleSideDrawerHandler}/>
                <SideDrawer 
                    isAuth = {this.props.isAuthenticated}
                    open={this.state.showSideDrawer} 
                    closed={this.sideDrawerClosedHandler}/>
                <main className={classes.content}>
                    {this.props.children}
                </main>
            </Aux>
        )
    }
};

const mapStateToProps = state => {
    return {
        isAuthenticated : state.auth.token !== null
    }
}



export default connect(mapStateToProps)(Layout);